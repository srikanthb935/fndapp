import { HomePageComponent } from './home-page.component';
import { HomeSliderComponent } from './components/home-slider/home-slider.component';

export const HomeModule = angular.module(
  "fnd.home",  [])
  .component( HomePageComponent.NAME, new HomePageComponent() )
  .component( HomeSliderComponent.NAME, new HomeSliderComponent() )
  .name;