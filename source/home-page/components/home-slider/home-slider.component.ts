export class HomeSliderController {
  static NAME = "homeSliderController";

  /* @ngInject */
  constructor() { }

  $onInit() {
  }
}

export class HomeSliderComponent implements ng.IComponentOptions {
  static NAME = "homeSliderComponent";
  template = require("./home-slider.component.html");
  controller = HomeSliderController;
  controllerAs = "vm";
}
