import { RoutingConfig } from "./index.routing";
import { HeaderModule } from "./header";
import { HomeModule } from "./home-page";

let uiRouterModule = require("angular-ui-router");
let uiBootstrapModule = require("angular-ui-bootstrap");

require("hammerjs");

let fndModule = angular.module("freshndesi", [
    uiRouterModule, 
    uiBootstrapModule,
    HeaderModule,
    HomeModule
  ])
  .config(RoutingConfig);

export default fndModule;
