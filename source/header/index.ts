import { HeaderComponent } from './header.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { FeatureComponent } from './components/features/feature.component';
 
export const HeaderModule = angular.module(
  "fnd.header",  [])
  .component( HeaderComponent.NAME, new HeaderComponent() )
  .component( NavigationComponent.NAME, new NavigationComponent() )
  .component( FeatureComponent.NAME, new FeatureComponent() )
  .name;