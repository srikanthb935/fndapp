export class HeaderController {
  static NAME = "headerController";

  public contactData: any;

  /* @ngInject */
  constructor() { }

  $onInit() {
    this.contactData = {
      watsapp: "+91 733 722 7722",
      mobile: "+91 733 744 7744",
      mailId: "info@freshndesi.com",
      message: "Dedicated to Preach, Practice and Promote Natural Marming Agricultural Methods"
    };
  }
}

export class HeaderComponent implements ng.IComponentOptions {
  static NAME = "headerComponent";
  template = require("./header.component.html");
  controller = HeaderController;
  controllerAs = "vm";
}
