export class NavigationController {
  static NAME = "navigationController";

  public navLinks: Array<object>;

  /* @ngInject */
  constructor() { }

  $onInit() {
    this.navLinks = [
      { name: "Home", link: "#/home" },
      { name: "Products", link: "#/products" },
      { name: "About", link: "#/about" },
      { name: "Contact Us", link: "#/contactus" }
    ];
  }
}

export class NavigationComponent implements ng.IComponentOptions {
  static NAME = "navigationComponent";
  template = require("./navigation.component.html");
  controller = NavigationController;
  controllerAs = "vm";
}
