export class FeatureController {
  static NAME = "featureController";

  public features: Array<object>;

  /* @ngInject */
  constructor() { }

  $onInit() {
    this.features = [
      { label: "Door Delivery (6pm - 8 pm)", icon: "icon-truck"},
      { label: "Support 24/7 For Clients", icon: "icon-earphones-alt"},
      { label: "100% Satisfaction Guarantee", icon: "icon-like"}
     ];
  }
}

export class FeatureComponent implements ng.IComponentOptions {
  static NAME = "featureComponent";
  template = require("./feature.component.html");
  controller = FeatureController;
  controllerAs = "vm";
}
