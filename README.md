#Angular Components with TypeScript

Prerquisites:
- Install npm globally
- Install Visual Studio Code (To transpile the Typescript code to JS)


1) Move to respective folder in the terminal.
2) npm install yarn -g
3) yarn
4) npm run start

Note:
  Make sure all the packages mentioned above say npm, bower, gulp were installed globally in your system before running the commands.
